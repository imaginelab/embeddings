'''
visualize_mesh_02.py
Using turtle to visualize mesh
'''

import turtle
import sys
import numpy as np
import re

if __name__ == '__main__':
    ## * Read data from file *

    if len(sys.argv) != 2: 
        print('Usage: visualize_mesh_02.py FILE_WITH_MESH_DATA')
        exit(1)

    filepath = sys.argv[1]

    with open(filepath, 'r') as file: 
        # Ambient dimensions 
        amb_dim = int(file.readline().rstrip())

        if amb_dim != 2: 
            print(f'Ambient dimension == {amb_dim}. (Now only support 2)')
            exit(1)

        # Number of vertices 
        nb_vertices = int(file.readline().rstrip())
        
        # Read coordinate of vertices and add them in a list
        vertices = []
        for i in range(nb_vertices): 
            splitted = re.split(' |,|;', file.readline().rstrip())
            x_str, y_str, geom = list(filter(None, splitted))
            x, y = float(x_str), float(y_str)
            vertices.append((x, y))

        # Number of triangles
        nb_triangles = int(file.readline().rstrip())
        # Edge set: for plotting edges with turtle or other tools 
        edges = set()

        # For each triangle, identify its edges by the tuple (vert1, vert2) where indices vert1 < vert2. Then add it to a set
        for i in range(nb_triangles): 
            v1_str, v2_str, v3_str = file.readline().rstrip().split(' ')
            v1, v2, v3 = int(v1_str), int(v2_str), int(v3_str)
            edges.add((min(v1, v2), max(v1, v2)))
            edges.add((min(v1, v3), max(v1, v3)))
            edges.add((min(v2, v3), max(v2, v3)))

        file.close()

    

    ## * Transform the vertices for visualization *

    # Translate so that the center is at origin
    vertices_tr = np.array(vertices)
    centroid = np.mean(vertices_tr, axis = 0)
    vertices_tr -= centroid

    # Scale so that the full canvas is filled 
    width, height = turtle.screensize()
    x_furthest, y_furthest = np.max(np.abs(vertices_tr), axis = 0)
    scale_factor = min(width / x_furthest, height / y_furthest) * 0.7
    vertices_tr *= scale_factor
    


    ## * Visualize with turtle * 

    scr = turtle.getscreen()
    cursor = turtle.Turtle(visible = False)    # Hide turtle pointer
    

    turtle.tracer(0, 0)    # Stop refreshing every time. 
    
    for edge in edges: 
        start_ind, end_ind = edge
        x_start, y_start = vertices_tr[start_ind]
        x_end, y_end = vertices_tr[end_ind]

        cursor.up()
        cursor.goto(x_start, y_start)
        cursor.down()
        cursor.goto(x_end, y_end)
        turtle.hideturtle()    # Hide turtle pointer 

    turtle.update()    # Update for the final graphic
    cursor.screen.mainloop()    # Pause the screen 