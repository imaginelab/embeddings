# Visualize super triangle to make sure it works

import numpy as np
import matplotlib.pyplot as plt 

if __name__ == '__main__':
    points = []
    with open('./random_points_01.txt') as f: 
        for line in f: 
            x_str, y_str = line.split(',')
            x = float(x_str)
            y = float(y_str)
            points.append([x, y])
        f.close()

    points = np.array(points)

    plt.axis('square')
    plt.xlim((-2, 2))
    plt.ylim((-2, 3))
    
    plt.scatter(points[:, 0], points[:, 1])
    plt.plot(
        [-1.76324, 1.77249, 1.51037, -1.76324], 
        [1.1257, 2.86439, -1.06699, 1.1257], 
        color = 'red', 
    )
    
    plt.show()