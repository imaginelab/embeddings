'''
visualize_mesh_03.py
-- Plot the histogram of mesh quality
-- Visualize mesh with a heat map of mesh quality 
'''

import sys 
import numpy as np 
import re 
from shapely.geometry.polygon import Polygon
from descartes import PolygonPatch
from matplotlib import pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.cm import ScalarMappable

if __name__ == '__main__': 
    ## * Read data from file *

    if len(sys.argv) != 2: 
        print('Usage: visualize_mesh_03.py FILE_WITH_MESH_DATA')
        exit(1)

    filepath = sys.argv[1]

    with open(filepath, 'r') as file: 
        # Ambient dimensions 
        amb_dim = int(file.readline().rstrip())

        if amb_dim != 2: 
            print(f'Ambient dimension == {amb_dim}. (Now only support 2)')
            exit(1)

        # Number of vertices 
        nb_vertices = int(file.readline().rstrip())
        
        # Read coordinate of vertices and add them in a list
        vertices = []
        for i in range(nb_vertices): 
            splitted = re.split(' |,|;', file.readline().rstrip())
            x_str, y_str, geom = list(filter(None, splitted))[0:3]
            x, y = float(x_str), float(y_str)
            vertices.append((x, y))

        # Number of triangles
        nb_triangles = int(file.readline().rstrip())
        # List of triangles 
        triangles = []
        # List of mesh qualities associated with each triangle 
        qualities = []

        # For each triangle, identify its edges by the tuple (vert1, vert2) where indices vert1 < vert2. Then add it to a set
        for i in range(nb_triangles): 
            splitted = re.split(' |,|;', file.readline().rstrip())
            v1_str, v2_str, v3_str, qual_str = list(filter(None, splitted))[0:4]
            v1, v2, v3 = int(v1_str), int(v2_str), int(v3_str)
            qual = float(qual_str)
            triangles.append((v1, v2, v3))
            qualities.append(qual)

        suptitle = file.readline().rstrip()

        file.close()

    
    ## * Histogram of mesh qualities *
    # plt.hist(qualities, bins = 20)
    # plt.show()

    # qualities = np.array(qualities)
    # print(f'Number of triangles = {len(triangles)}')
    # print(np.sum(qualities >= 0.8) / len(qualities))


    ## * Plot mesh * 

    colordict = {
        'red': (
            (0, 0, 0), 
            (0.15, 70/255, 70/255), 
            (0.3, 140/255, 140/255), 
            (0.45, 190/255, 190/255), 
            (0.6, 239/255, 239/255), 
            (0.8, 236/255, 236/255), 
            (1, 193/255, 193/255),
        ), 
        'green': (
            (0, 0, 0), 
            (0.15, 109/255, 109/255), 
            (0.3, 217/255, 217/255), 
            (0.45, 211/255, 211/255), 
            (0.60, 204/255, 204/255), 
            (0.8, 163/255, 163/255), 
            (1, 51/255, 51/255), 
        ), 
        'blue': (
            (0, 0, 0), 
            (0.15, 129/255, 129/255), 
            (0.3, 213/255, 213/255), 
            (0.45, 150/255, 150/255), 
            (0.6, 87/255, 87/255), 
            (0.8, 11/255, 11/255), 
            (1, 54/255, 54/255),
        ), 
    }

    colormap = LinearSegmentedColormap('heat_map_for_mesh_quality', colordict)

    map_helper = ScalarMappable(cmap = colormap)


    fig, axs = plt.subplots(1, 2, figsize = (11, 5))
    axs = axs.reshape(-1, )

    fig.suptitle(suptitle)
    
    axs[0].hist(qualities, bins = 20)
    axs[0].set_title(f'Distribution of mesh quality. \nMean = {np.mean(qualities):.3f}, stddev = {np.std(qualities):.3f}')

    for i in range(len(triangles)): 
        v1_idx, v2_idx, v3_idx = triangles[i]
        v1_coord = vertices[v1_idx]
        v2_coord = vertices[v2_idx]
        v3_coord = vertices[v3_idx]
        qual = qualities[i]
        r, g, b, a = map_helper.to_rgba(qual, norm = False)
        ring_mixed = Polygon([v1_coord, v2_coord, v3_coord])
        ring_patch = PolygonPatch(ring_mixed, facecolor = (r, g, b), edgecolor = 'black')
        axs[1].add_patch(ring_patch)
    
    axs[1].set_title('Heat map of mesh')
    axs[1].set_xlim(0, 10)
    axs[1].set_ylim(0, 10)

    fig.colorbar(map_helper)

    plt.subplots_adjust(top = 0.82)
    plt.show()