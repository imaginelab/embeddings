import numpy as np

if __name__ == '__main__': 
    nb_points = 1000
    random_points = np.random.uniform(size = (nb_points, 2))
    random_points[:, 1] *= 2    # so that y-coord ranges from 0 to 2
    with open('random_points.txt', 'w') as f: 
        for point in random_points: 
            f.write(f'{point[0]}, {point[1]}\n')
    f.close()