#include "basics.h"
#include "metric.h"
#include "math.h"

#define SHORT_EDGE_LEN sqrt(2) / 2
#define LONG_EDGE_LEN sqrt(2)
#define TARGET_QUALITY 0.8

#ifndef SCHEDULING_H
#define SCHEDULING_H

void collapse_all_edges(Topology *topology, MetricField *metricfield); 

void split_all_edges(Topology *topology, MetricField *metricfield); 

void swap_all_edges(Topology *topology, MetricField *metricfield); 

void smooth_all_vertices(Topology *topology, MetricField *metricfield); 

void perform_schedule(Topology *topology, MetricField *metricfield, int nb_iter = 10, bool smooth = true); 

#endif