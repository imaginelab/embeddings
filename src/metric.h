#include <Eigen/Core> 
#include <math.h>
#include <string>

using namespace std; 

#ifndef METRIC_H
#define METRIC_H

// Define the normalizing coefficient beta_n for eval mesh quality 
// Combining Eqn (2.2) and (2.17) in (Caplan 2.19), we obtain the value of beta_2 as a constant 
#define BETA 12 / sqrt(3)

// Define the relaxation factor omega for vertex smoothing 
// See Eqn (3.10) in (Caplan 2019)
#define OMEGA 0.2

// Modes of constructing the metricfield 
#define CONSTRUCT_BY_METRIC_TENSOR 1
#define CONSTRUCT_BY_PRINCIPALS 2    // i.e. by specifying principal lengths and principal directions 
#define CONSTRUCT_BY_EMBEDDING -1

// Modes of computing metric-related quantities
#define COMPUTE_BY_METRIC 1
#define COMPUTE_BY_EMBEDDING -1

// * Here are possible combinations of constructor mode and computation mode
// *             |  Construct by metric    |    Construct by embedding
// * ============|=========================|===============================
// * Compute by  |      Allowed            |            Allowed
// *   metric    |                         |       Use induced metric
// * ------------|-------------------------|-------------------------------
// * Compute by  |      NOT Allowed        |            allowed
// * embedding   |                         |

using namespace Eigen; 

double get_distance_Euclidean(double *coord1, double *coord2, int amb_dim = 2); 

double get_volume_Euclidean(double *coord1, double *coord2, double *coord3, int amb_dim = 2);

double get_quality_Euclidean(double *coord1, double *coord2, double *coord3, int amb_dim = 2); 



// Since we decided to compute the induced metric symbolically with sympy instead of doing auto-differtiation, a temporary solution is to create a class that associate embedding function and induced metric 
class Embedding {
public: 
    Embedding(double (*lift_func_arg)(double *coord), 
              string lift_func_tex_arg, 
              Matrix2d (*get_induced_metric_at_arg)(double *coord)); 

    // I did not declare the attributes private, because for now I can't deal with the messy notation of function pointers 
    double (*lift_func)(double *coord); 
    string lift_func_tex; 
    Matrix2d (*get_induced_metric_at)(double *coord);
};



class MetricField {
public: 
    // Only for CONSTRUCT_BY_PRINCIPALS, COMPUTE_BY_METRIC
    MetricField(
        short construct_mode_arg, 
        short compute_mode_arg, 
        Matrix2d (*get_principal_directions_at_arg)(double *coord), 
        Vector2d (*get_principal_lengths_at_arg)(double *coord)
    ); 

    // Only for CONSTRUCT_BY_METRIC_TENSOR, COMPUTE_BY_METRIC
    MetricField(
        short construct_mode_arg, 
        short compute_mode_arg, 
        Matrix2d (*get_metric_tensor_at_arg)(double *coord)
    ); 

    // DO NOT USE! Legacy only.
    // Construct by embedding function 
    // Now only support (x, y) --> (x, y, f(x, y))
    MetricField(
        short construct_mode_arg, 
        short compute_mode_arg, 
        double (*lift_func_arg)(double *coord)
    ); 

    // Construct by an Embedding object 
    // An embedding function is associated with an induced metric 
    MetricField(
        short construct_mode_arg, 
        short compute_mode_arg, 
        Embedding &embed
    ); 

    string get_description(); 

    bool is_construct_by_metric();  

    bool is_construct_by_embedding(); 

    bool is_compute_by_metric(); 

    bool is_compute_by_embedding(); 

    void set_compute_by_metric(); 

    void set_compute_by_embedding(); 

    Matrix2d get_metric_tensor_at(double *coord); 

    double get_distance_with_constant_metric(double *coord1, double *coord2, Matrix2d& metric); 

    double get_distance(double *coord1, double *coord2);  

    double get_volume_with_constant_metric(double *coord1, double *coord2, double *coord3, Matrix2d& metric); 

    double get_volume(double *coord1, double *coord2, double *coord3); 

    double get_quality_with_constant_metric(double *coord1, double *coord2, double *coord3, Matrix2d& metric); 

    double get_quality(double *coord1, double *coord2, double *coord3); 

private: 
    const short construct_mode_; 
    short compute_mode_; 
    Matrix2d (*get_metric_tensor_at_)(double *coord); 
    Matrix2d (*get_principal_directions_at_)(double *coord);
    Vector2d (*get_principal_lengths_at_)(double *coord);
    double (*lift_func_)(double *coord); 
    string lift_func_tex_; 
}; 

#endif


