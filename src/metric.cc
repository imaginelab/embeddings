#include "metric.h"
#include <Eigen/Core> 
#include <Eigen/LU>  
#include <algorithm>
#include <iostream> 
#include <math.h>
#include <string.h>
#include <string>
#include "error.h"

using namespace std; 
using namespace Eigen; 

// * =================
// * Warning: all functions in class MetricField except for get_distance_Euclidean(), get_volume_Euclidean() are defined only for 2D
// * =================

// Compute Euclidean distance for ANY dimension
double get_distance_Euclidean(double *coord1, double *coord2, int amb_dim) {
    double s = 0; 
    for (int i = 0; i < amb_dim; i++) {
        s += pow((*(coord1 + i) - *(coord2 + i)), 2); 
    }
    return sqrt(s); 
}



// Compute volume of a simplex with Euclidean metric 
// For triangles now, we use Heron's formula 
// The triangles can be in ANY dimension 
double get_volume_Euclidean(double *coord1, double *coord2, double *coord3, int amb_dim) {
    double dist1 = get_distance_Euclidean(coord1, coord2, amb_dim); 
    double dist2 = get_distance_Euclidean(coord2, coord3, amb_dim); 
    double dist3 = get_distance_Euclidean(coord1, coord3, amb_dim); 
    double semi = (dist1 + dist2 + dist3) / 2; 
    return sqrt(semi * (semi - dist1) * (semi - dist2) * (semi - dist3)); 
}



// Compute quality of a triangle with Euclidean metric
// The triangle can be in ANY dimension  
double get_quality_Euclidean(double *coord1, double *coord2, double *coord3, int amb_dim) {
    double volume = get_volume_Euclidean(coord1, coord2, coord3, amb_dim); 
    double dist1 = get_distance_Euclidean(coord1, coord2, amb_dim); 
    double dist2 = get_distance_Euclidean(coord1, coord3, amb_dim); 
    double dist3 = get_distance_Euclidean(coord2, coord3, amb_dim); 

    return BETA * volume / (dist1 * dist1 + dist2 * dist2 + dist3 * dist3); 
}



// * Constructor of MetricField by principal directions and lengths
// * =================
// * The elliptical representation of a metric tensor has 2 components: 
// *    - Principal directions (normalized to length 1) (Putting together these column vectors and we get the matrix Q)
// *    - Principal lengths (The diagonal matrix formed by principal lengths is denoted H)
// * The metric tensor is then computed by M = Q(H^(-2))Q^T (Caplan 2019 Eqn 2.10)
// * =================
// * The constructor requires 3 arguments 
//      - A matrix of principal directions (eigenvectors)
//      - A vector of principal lengths (eigenvalues)
//      - The mode of computing distance: BY_METRIC or BY_EMBEDDING
MetricField::MetricField(
    short construct_mode_arg, 
    short compute_mode_arg, 
    Matrix2d (*get_principal_directions_at_arg)(double *coord), 
    Vector2d (*get_principal_lengths_at_arg)(double *coord)
) : construct_mode_(construct_mode_arg), 
    compute_mode_(compute_mode_arg), 
    get_principal_directions_at_(get_principal_directions_at_arg), 
    get_principal_lengths_at_(get_principal_lengths_at_arg) 
{
    if (construct_mode_arg != CONSTRUCT_BY_PRINCIPALS) {
        char *msg = "Error: expected CONSTRUCT_BY_PRINCIPALS as construct-mode for MetricField"; 
        avro_throw(msg); 
    }
    if (compute_mode_arg != COMPUTE_BY_METRIC) {
        char *msg = "Error: expected COMPUTE_BY_METRIC as compute-mode for MetricField"; 
        avro_throw(msg); 
    }
}



// Constructor of MetricField by a metric tensor 
MetricField::MetricField(
    short construct_mode_arg, 
    short compute_mode_arg, 
    Matrix2d (*get_metric_tensor_at_arg)(double *coord) 
) : construct_mode_(construct_mode_arg), 
    compute_mode_(compute_mode_arg), 
    get_metric_tensor_at_(get_metric_tensor_at_arg) 
{
    if (construct_mode_arg != CONSTRUCT_BY_METRIC_TENSOR) {
        char *msg = "Error: expected CONSTRUCT_BY_METRIC_TENSOR as construct-mode for MetricField"; 
        avro_throw(msg); 
    }
    if (compute_mode_arg != COMPUTE_BY_METRIC) {
        char *msg = "Error: expected COMPUTE_BY_METRIC as compute-mode for MetricField"; 
        avro_throw(msg); 
    }
}



// DO NOT USE! Legacy only.
// Constructor of MetricField by an embedding function 
// Now only support (x, y) --> (x, y, f(x, y))
MetricField::MetricField(
    short construct_mode_arg, 
    short compute_mode_arg, 
    double (*lift_func_arg)(double *coord)
) : construct_mode_(construct_mode_arg), 
    compute_mode_(compute_mode_arg), 
    lift_func_(lift_func_arg)
{
    if (construct_mode_arg != CONSTRUCT_BY_EMBEDDING) {
        char *msg = "Error: expected CONSTRUCT_BY_EMBEDDING as construct-mode for MetricField"; 
        avro_throw(msg);
    }
    if (compute_mode_arg != COMPUTE_BY_EMBEDDING) {
        char *msg = "Error: please instantiate MetricField with an Embedding object if you wish to construct by embedding but compute by induced metric."; 
        avro_throw(msg); 
    }
}



// Constructor of MetricField by an Embedding class
// A lift function and the induced metric is stored in Embedding class 
MetricField::MetricField(
    short construct_mode_arg, 
    short compute_mode_arg, 
    Embedding &embed
) : construct_mode_(construct_mode_arg), 
    compute_mode_(compute_mode_arg), 
    lift_func_(embed.lift_func), 
    get_metric_tensor_at_(embed.get_induced_metric_at), 
    lift_func_tex_(embed.lift_func_tex)
{
    if (construct_mode_arg != CONSTRUCT_BY_EMBEDDING) {
        char *msg = "Error: expected CONSTRUCT_BY_EMBEDDING as construct-mode for MetricField"; 
        avro_throw(msg);
    }
}



// Get a short description of the current metric field
string MetricField::get_description() {
    string res = ""; 
    if (this->lift_func_tex_ != "") {
        res += this->lift_func_tex_; 
    }

    if (this->is_compute_by_embedding()) {
        res += ", compute by embedding"; 
    }
    else if (this->is_compute_by_metric()) {
        res += ", compute by metric"; 
    }

    return res; 
}



// True if the mode of constructor is by metric  
bool MetricField::is_construct_by_metric() {
    return this->construct_mode_ > 0; 
}



// True if the mode of constructor is by embedding 
bool MetricField::is_construct_by_embedding() {
    return this->construct_mode_ < 0; 
}



// True if the mode of computing is by metric 
bool MetricField::is_compute_by_metric() {
    return this->compute_mode_ > 0; 
}



// True if the mode of computing is by embedding 
bool MetricField::is_compute_by_embedding() {
    return this->compute_mode_ < 0; 
}



// Set compute mode to embedding 
void MetricField::set_compute_by_metric() {
    if (this->get_metric_tensor_at_ == nullptr) {
        char *msg = "Metric tensor is not initialized when constructing the metric field. Please double check your constructor."; 
        avro_throw(msg); 
    }
    this->compute_mode_ = COMPUTE_BY_METRIC; 
}



// Set compute mode to metric
void MetricField::set_compute_by_embedding() {
    if (this->lift_func_ == nullptr) {
        char *msg = "Embedding lift function is not initialized when constructing the metric field. Please double check your constructor."; 
        avro_throw(msg); 
    }
    this->compute_mode_ = COMPUTE_BY_EMBEDDING; 
}



// Get metric tensor at a point 
Matrix2d MetricField::get_metric_tensor_at(double *coord) {
    Matrix2d res; 

    if (this->construct_mode_ == CONSTRUCT_BY_PRINCIPALS) {
        Matrix2d principal_dirs = this->get_principal_directions_at_(coord); 
        Vector2d principal_lens = this->get_principal_lengths_at_(coord);

        if (principal_lens(0) == 0 || principal_lens(1) == 0) {
            cout << "In MetricField::get_principal_lengths_at(): 0 principle lengths is not allowed! Exiting..." << endl; 
            exit(1); 
        }

        
        if (!principal_dirs.determinant()) {
            cout << "In MetricField::get_principal_directions_at(): The matrix of principal directions is degenerate (non-invertible). Exiting..." << endl; 
            exit(1); 
        }


        Vector2d eigen_value_diag = principal_lens.array().square().cwiseInverse();
        res = principal_dirs * eigen_value_diag.asDiagonal() * principal_dirs.transpose(); 

    } 

    else if (this->construct_mode_ == CONSTRUCT_BY_METRIC_TENSOR) {
        res = this->get_metric_tensor_at_(coord); 
    }

    else if (this->construct_mode_ == CONSTRUCT_BY_EMBEDDING && 
             this->compute_mode_ == COMPUTE_BY_METRIC) {
        res = this->get_metric_tensor_at_(coord); 
    }

    else {
        char *msg = "Cannot get metric tensor. You must have done something wrong in the constructor."; 
        avro_throw(msg); 
    }
    
    
    // Report error if the metric tensor has 0 determinant
    if (!res.determinant()) {
        // avro_throw(false); 
        cout << "The metric tensor has 0 determinant at (" + to_string(coord[0]) + ", " + to_string(coord[1]) + "). Exiting..." << endl;
        exit(1);  
    }

    return res; 
}



// Compute distance with a constant metric 
double MetricField::get_distance_with_constant_metric(double *coord1, double *coord2, Matrix2d& metric) {
    Vector2d diff(*coord2 - *coord1, *(coord2 + 1) - *(coord1 + 1)); 
    return sqrt(diff.transpose() * metric * diff); 
}



// Approximate distance under a varying metric field (Caplan 2019 Eqn 2.12)
double MetricField::get_distance(double *coord1, double *coord2) {
    // Compute distance with defined metric field
    if (this->is_compute_by_metric()) {
        Matrix2d metric1 = this->get_metric_tensor_at(coord1); 
        Matrix2d metric2 = this->get_metric_tensor_at(coord2); 

        double dist1 = this->get_distance_with_constant_metric(coord1, coord2, metric1); 
        double dist2 = this->get_distance_with_constant_metric(coord1, coord2, metric2); 
        double ratio = dist1 / dist2; 

        if (ratio == 1) {return dist1; }
        else if (ratio == 0) {return 0; }

        return dist1 * (ratio - 1) / (ratio * log(ratio));
    }

    // Compute distance within the embedded space 
    else if (this->is_construct_by_embedding() && this->is_compute_by_embedding()) {
        double coord1_embed[3] = {coord1[0], coord1[1], this->lift_func_(coord1)}; 
        double coord2_embed[3] = {coord2[0], coord2[1], this->lift_func_(coord2)}; 
        return get_distance_Euclidean(coord1_embed, coord2_embed, 3); 
    }

    char *msg = "Cannot construct with metric and compute with embedding. You are on your own"; 
    avro_throw(msg); 
    return -1;      
}



// Compute volume of a simplex with constant metric 
double MetricField::get_volume_with_constant_metric(double *coord1, double *coord2, double *coord3, Matrix2d& metric) {
    return sqrt(metric.determinant()) * get_volume_Euclidean(coord1, coord2, coord3); 
}



// Approximate volume of a simplex under a varying metric field (Caplan 2019 Eqn 2.14)
double MetricField::get_volume(double *coord1, double *coord2, double *coord3) {
    // Compute volume with defined metric field 
    if (this->is_compute_by_metric()) {
        // Get metric tensors at each of 3 points
        Matrix2d metric1 = this->get_metric_tensor_at(coord1); 
        Matrix2d metric2 = this->get_metric_tensor_at(coord2); 
        Matrix2d metric3 = this->get_metric_tensor_at(coord3); 

        // Compute each determinant 
        double det1 = metric1.determinant(); 
        double det2 = metric2.determinant(); 
        double det3 = metric3.determinant(); 

        // Find the max determinant 
        double det; 
        det = max(det1, det2); 
        det = max(det, det3); 

        return sqrt(det) * get_volume_Euclidean(coord1, coord2, coord3);
    }

    // Compute volume within embedded space 
    else if (this->is_construct_by_embedding() && this->is_compute_by_embedding()) {
        double coord1_embed[3] = {coord1[0], coord1[1], this->lift_func_(coord1)}; 
        double coord2_embed[3] = {coord2[0], coord2[1], this->lift_func_(coord2)}; 
        double coord3_embed[3] = {coord3[0], coord3[1], this->lift_func_(coord3)}; 
        return get_volume_Euclidean(coord1_embed, coord2_embed, coord3_embed, 3); 
    }

    char *msg = "Cannot construct with metric and compute with embedding. You are on your own"; 
    avro_throw(msg); 
    return -1;        
}



// Compute mesh quality with a constant metric (Caplan 2019 Eqn 2.17)
double MetricField::get_quality_with_constant_metric(double *coord1, double *coord2, double *coord3, Matrix2d& metric) {
    double volume = this->get_volume_with_constant_metric(coord1, coord2, coord3, metric); 
    double dist1 = this->get_distance_with_constant_metric(coord1, coord2, metric); 
    double dist2 = this->get_distance_with_constant_metric(coord1, coord3, metric); 
    double dist3 = this->get_distance_with_constant_metric(coord2, coord3, metric); 

    return BETA * volume / (dist1 * dist1 + dist2 * dist2 + dist3 * dist3); 
}



// Compute mesh quality under varying metric field
double MetricField::get_quality(double *coord1, double *coord2, double *coord3) {
    // Compute quality with defined metric field
    if (this->is_compute_by_metric()) {
        // Get metric tensors at each of 3 points
        Matrix2d metric1 = this->get_metric_tensor_at(coord1); 
        Matrix2d metric2 = this->get_metric_tensor_at(coord2); 
        Matrix2d metric3 = this->get_metric_tensor_at(coord3); 

        // Compute each determinant 
        double det1 = metric1.determinant(); 
        double det2 = metric2.determinant(); 
        double det3 = metric3.determinant(); 

        // Find the metric with the max determinant 
        Matrix2d metric; 
        if (det1 >= det2 && det1 >= det3) {
            metric = metric1; 
        }
        else if (det2 >= det1 && det2 >= det3) {
            metric = metric2; 
        }
        else {
            metric = metric3; 
        }

        return this->get_quality_with_constant_metric(coord1, coord2, coord3, metric); 
    }

    // Compute quality within embedded space 
    else if (this->is_construct_by_embedding() && this->is_compute_by_embedding()){
        double coord1_embed[3] = {coord1[0], coord1[1], this->lift_func_(coord1)}; 
        double coord2_embed[3] = {coord2[0], coord2[1], this->lift_func_(coord2)}; 
        double coord3_embed[3] = {coord3[0], coord3[1], this->lift_func_(coord3)}; 
        return get_quality_Euclidean(coord1_embed, coord2_embed, coord3_embed, 3); 
    }

    char *msg = "Cannot construct with metric and compute with embedding. You are on your own"; 
    avro_throw(msg); 
    return -1;

}



Embedding::Embedding(
    double (*lift_func_arg)(double *coord), 
    string lift_func_tex_arg, 
    Matrix2d (*get_induced_metric_at_arg)(double *coord)
) : lift_func(lift_func_arg), 
    lift_func_tex(lift_func_tex_arg), 
    get_induced_metric_at(get_induced_metric_at_arg){}