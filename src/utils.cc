#include "basics.h"
#include "utils.h"
#include <fstream>
#include <iostream> 
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include "error.h"
#include <iomanip>

using namespace std; 


// Read vertices and triangles from a file 
void read_topology(string filename, Topology *topology) {
    fstream myfile; 
    myfile.open(filename, ios::in); 
    string line; 

    if (myfile.fail()) {
        cout << "In read_pointset_and_triangulation(): Fail to open " << filename << endl; 
        exit(1); 
    }

    auto *pointset = topology->pointset(); 

    // Part I: Read vertices
    
    // Read the number of ambient dimensions 
    getline(myfile, line); 
    const int amb_dim = stoi(line); 
    pointset->amb_dim(amb_dim); 

    // Read the number of vertices 
    getline(myfile, line);
    int nb_points = stoi(line) ; 

    // Read the coordinate and the "geometry" attribute of each point 
    for (int i = 0; i < nb_points; i++) {
        getline(myfile, line); 

        char *line_c_str = new char[line.length() + 1];
        strcpy(line_c_str, line.c_str()); 

        // Get the coordinate 
        double coordinate[amb_dim];
        for (int j = 0; j < pointset->amb_dim(); j++) {
            // Parse the current line and get the current component
            char *component_str; 
            if (j == 0) {
                component_str = strtok(line_c_str, " ,;"); 
            } 
            else {
                component_str = strtok(NULL, " ,;"); 
            }

            // Convert the string of current component to double
            coordinate[j] = strtod(component_str, NULL); 
        }

        // Insert the current vertex 
        double *coord = coordinate; 
        pointset->add_vertex(coord); 

        // Read the current geometry 
        char *geom_str = strtok(NULL, " ,;"); 
        short geom = (short)strtod(geom_str, NULL); 
        pointset->add_geometry(geom); 
    }
    

    // Part II: Read triangles 
    
    // Read number of triangles 
    getline(myfile, line); 
    int nb_triangles = stoi(line); 

    // Read each triangle and store them as dictionary
    for (int i = 0; i < nb_triangles; i++) {
        getline(myfile, line); 

        char *line_c_str = new char[line.length() + 1];
        strcpy(line_c_str, line.c_str()); 

        // Get the three coordinate indices by parsing
        char *vert_1_idx_str = strtok(line_c_str, " ,;"); 
        char *vert_2_idx_str = strtok(NULL, " ,;"); 
        char *vert_3_idx_str = strtok(NULL, " ,;"); 

        // Convert indices from char * to int
        int vert_1_idx = (int)strtod(vert_1_idx_str, NULL); 
        int vert_2_idx = (int)strtod(vert_2_idx_str, NULL); 
        int vert_3_idx = (int)strtod(vert_3_idx_str, NULL); 

        // Make sure the three vertices are in counter-clockwise order, and then insert 
        topology->add_triangle(vert_1_idx, vert_2_idx, vert_3_idx); 
    }

}


// Reverse a directed edge 
edge_t rev(edge_t edge) {
    return {edge[1], edge[0]}; 
}


// Print the indices of vertices of a triangle 
void print_triangle(triangle_t triangle) {
    cout << "Vertices of triangle: " << triangle[0] << ", " << triangle[1] << ", " << triangle[2] << endl; 
}



// Output info of vertices and triangulations into a file 
// If print_quality is set to true, then include info of mesh quality in the exported file 
void export_mesh_info(string filename, Topology *topology, bool print_quality, short eval_mode, MetricField *metricfield, double time_elapsed) {
    // Error checking 
    if (print_quality && !metricfield) {
        char *msg = "Require a non-null pointer of MetricField if export info about mesh quality"; 
        avro_throw(msg); 
    }

    Vertices *pointset = topology->pointset(); 
    fstream myfile; 
    myfile.open(filename, ios::out);

    // Print ambient dimension
    myfile << pointset->amb_dim() << endl; 

    // Print number of vertices 
    myfile << pointset->nb() << endl; 

    // For each vertex, print its coordinate and geometry
    for (int i = 0; i < pointset->nb(); i++) {
        double *coord = pointset->get_vertex(i); 
        for (int j = 0; j < pointset->amb_dim(); j++) {
            if (j != 0) {myfile << ", "; }
            myfile << *(coord + j); 
        }
        myfile << "; " << pointset->get_geometry(i) << endl; 
    }

    // Print number of triangles 
    myfile << topology->nb() << endl; 

    // !! ATTENTION !!
    // For a mesh created by embedding, we want to evaluate the final mesh qualities using the mode specified by `eval_mode`. 
    // In this way, we are using the same ruler for evaluation
    bool toggled = false; 
    if (metricfield && metricfield->is_compute_by_embedding() && eval_mode == EVAL_BY_METRIC) {
        toggled = true; 
        metricfield->set_compute_by_metric(); 
    }
    else if(metricfield && metricfield->is_compute_by_metric() && eval_mode == EVAL_BY_EMBEDDING) {
        toggled = true; 
        metricfield->set_compute_by_embedding(); 
    }

    // Print the indices of vertices of each triangle
    topology->start_iterator(); 
    while(topology->has_triangle()) {
        triangle_t triangle = topology->next_triangle(); 
        myfile << triangle[0] << " " << triangle[1] << " " << triangle[2]; 
        // print mesh quality is specified 
        if (print_quality) {
            myfile << "; " << topology->get_quality(triangle, metricfield); 
        }
        myfile << endl;
    }

    // If compute_mode is toggled, change it back 
    if (metricfield && toggled) {
        if (eval_mode == EVAL_BY_METRIC) {
            metricfield->set_compute_by_embedding(); 
        }
        else if (eval_mode == EVAL_BY_EMBEDDING) {
            metricfield->set_compute_by_metric(); 
        }
        toggled = false; 
    }

    // Export a short description for printing title in visualization 
    if (metricfield) {
        myfile << metricfield->get_description() << ", "; 
        if (eval_mode == EVAL_BY_METRIC) {
            myfile << "eval by metric" << ", "; 
        }
        else if (eval_mode == EVAL_BY_EMBEDDING) {
            myfile << "eval by embedding" << ", "; 
        }

        // If specified, add `time_elapsed` in the description 
        if (time_elapsed > 0) { 
            myfile << fixed << setprecision(2) << "time elapsed = " << time_elapsed; 
        }


        myfile << endl;  
    }
}



// Visualize a mesh 
void visualize_mesh(Topology *topology, bool fork_process) {
    // Prepare a temporary file that stores the triangulation
    time_t rawtime = time(nullptr);
    string filename = "temp_mesh_" + to_string(rawtime) + ".txt"; 

    // Export mesh info to a temporary file
    export_mesh_info(filename, topology); 

    string exec_str = "python visualize_mesh_02.py "; 
    string rm_str = "rm -f "; 

    // Remain in the same process if specified
    if (!fork_process) {
        // Visualize mesh with Python script
        system((exec_str + filename).c_str());
        // Remove the temporary file
        system((rm_str + filename).c_str()); 
        return; 
    }

    // Fork new process if specified 
    else if (fork() == 0) {
        system((exec_str + filename).c_str());
        system((rm_str + filename).c_str()); 
    }
}

