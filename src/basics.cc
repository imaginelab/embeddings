#include "basics.h"
#include "metric.h"
#include <iostream> 
#include "predicates/predicates.h"
#include <time.h>
#include <stdlib.h>

using namespace std; 

// Constructor 
Vertices::Vertices(int amb_dim_arg) : amb_dim_(amb_dim_arg) {}


// The number of vertices in the current pointsize
int Vertices::nb() {
    // Each coordinate takes amb_dim_ entries to store 
    return this->vertices_.size() / this->amb_dim_; 
}


// Get the coordinate of the idx^th vertex in the current pointset 
// The result is presented as a pointer. To get each component of coordinate, use dereference operator
double *Vertices::get_vertex(int idx) {
    return &(this->vertices_[this->amb_dim_ * idx]); 
}


// Add new vertex to the pointset 
void Vertices::add_vertex(double *coord) {
    for (int i = 0; i < this->amb_dim_; i++) {
        this->vertices_.push_back(*(coord + i)); 
    }
}


// Get the geometry of a vertex 
short Vertices::get_geometry(int idx) {
    return this->geometry_[idx]; 
}


// Add the geometry of a new vertex 
void Vertices::add_geometry(short geom) {
    this->geometry_.push_back(geom); 
}


// Get ambient dimension 
int Vertices::amb_dim() {
    return this->amb_dim_; 
}


// Set ambient dimension 
int Vertices::amb_dim(int amb_dim_arg) {
    this->amb_dim_ = amb_dim_arg; 
    return this->amb_dim_; 
}


// Given an edge, infer the geometry type for any point between them
short Vertices::infer_geometry(edge_t edge) {
    short geom1 = this->get_geometry(edge[0]); 
    short geom2 = this->get_geometry(edge[1]); 

    // If two endpoints are on the same corner, the same boundary, or the same interior, then 
    if (geom1 == geom2) {return geom1; }

    // Discuss the cases where the first endpoint is on corner

    else if (geom1 == 1) {
        if (geom2 == -1 || geom2 == 2) {return -1; }
        if (geom2 == 4 || geom2 == -4) {return -4; }
    }

    else if (geom1 == 2) {
        if (geom2 == 1 || geom2 == -1) {return -1; }
        if (geom2 == 3 || geom2 == -2) {return -2; }
    }

    else if (geom1 == 3) {
        if (geom2 == 2 || geom2 == -2) {return -2; }
        if (geom2 == 4 || geom2 == -3) {return -3; }
    }

    else if (geom1 == 4) {
        if (geom2 == 3 || geom2 == -3) {return -3; }
        if (geom2 == -4 || geom2 == 1) {return -4; }
    }

    // Discuss the cases where the first endpoint in on boundary 

    else if (geom1 == -1) {
        if (geom2 == 1 || geom2 == 2) {return -1; }
    }

    else if (geom1 == -2) {
        if (geom2 == 2 || geom2 == 3) {return -2; }
    }

    else if (geom1 == -3) {
        if (geom2 == 3 || geom2 == 4) {return -3; }
    }

    else if (geom1 == -4) {
        if (geom2 == 4 || geom2 == 1) {return -4; }
    }

    // For any other case, any point on this edge must be on interior
    return 0; 
}


// Mark a vertex as removed 
void Vertices::remove(int idx) {
    this->removed_.insert(idx); 
}


// Check whether a vertex exists in the current mesh
bool Vertices::exists(int idx) {
    return idx < this->nb() && this->removed_.count(idx) == 0; 
}




// Constructor 
Topology::Topology(Vertices *pointset_arg) : pointset_(pointset_arg) {} 

// The number of triangles in the current topology 
int Topology::nb() {
    // Each triangle is queried by an edge thus stored 3 times 
    return this->triangles_dict_.size() / 3; 
}


// Get the pointer to the pointset relevant to the current topology 
Vertices *Topology::pointset() {
    return this->pointset_; 
}


// Query a triangle by a directed edge (clockwise direction)
triangle_t Topology::get_triangle(edge_t edge) {
    return this->triangles_dict_[edge]; 
}


// Add a triangle to collection by specifying the indices of vertices
void Topology::add_triangle(int vert_1_idx, int vert_2_idx, int vert_3_idx) {
    Vertices *pointset = this->pointset(); 

    if (pointset->amb_dim() != 2) {
        cout << "In Topology::add_triangle(): Now we only support orient2d(), but the ambient dimension is " << pointset->amb_dim() << ". Exiting..." << endl; 
        exit(1); 
    }

    double *p1 = pointset->get_vertex(vert_1_idx); 
    double *p2 = pointset->get_vertex(vert_2_idx); 
    double *p3 = pointset->get_vertex(vert_3_idx); 

    // Make sure the three vertices are in counter-clockwise order when inserting the triangle 
    if (orient2d(p1, p2, p3) > DET_THRESHOLD) {
        
        // The triangle should be queriable by all 3 edges 
        
        this->triangles_dict_.insert(
            pair<edge_t, triangle_t>(
                {vert_1_idx, vert_2_idx}, 
                {vert_1_idx, vert_2_idx, vert_3_idx}
            ) 
        ); 

        this->triangles_dict_.insert(
            pair<edge_t, triangle_t>(
                {vert_2_idx, vert_3_idx}, 
                {vert_1_idx, vert_2_idx, vert_3_idx}
            ) 
        ); 

        this->triangles_dict_.insert(
            pair<edge_t, triangle_t>(
                {vert_3_idx, vert_1_idx}, 
                {vert_1_idx, vert_2_idx, vert_3_idx}
            ) 
        ); 
    } 
    else if (orient2d(p1, p2, p3) < -DET_THRESHOLD) {
        this->triangles_dict_.insert(
            pair<edge_t, triangle_t>(
                {vert_1_idx, vert_3_idx}, 
                {vert_1_idx, vert_3_idx, vert_2_idx}
            ) 
        ); 

        this->triangles_dict_.insert(
            pair<edge_t, triangle_t>(
                {vert_3_idx, vert_2_idx}, 
                {vert_1_idx, vert_3_idx, vert_2_idx}
            ) 
        ); 

        this->triangles_dict_.insert(
            pair<edge_t, triangle_t>(
                {vert_2_idx, vert_1_idx}, 
                {vert_1_idx, vert_3_idx, vert_2_idx}
            ) 
        ); 
    } 
    else {
        // Report error if three points are colinear 
        cout << "In Topology::add_triangle(): vertices " << vert_1_idx << ", " << vert_2_idx << ", " << vert_3_idx << " are colinear and thus do not form a valid triangle. Exiting..." << endl; 
        exit(1); 
    }
}



// Delete a triangle by providing an identifying edge 
void Topology::delete_triangle(edge_t edge) {
    int third_vert = this->get_third_vertex(edge); 
    this->triangles_dict_.erase(edge); 
    this->triangles_dict_.erase({edge[1], third_vert}); 
    this->triangles_dict_.erase({third_vert, edge[0]});
}



// Given an identifying edge of a triangle, return the index of the third vertex
int Topology::get_third_vertex(edge_t edge) {
    // Report error if the edge is not in current topoplogy
    if (!this->has_edge(edge)) {
        cout << "In Topology::get_third_vertex(): edge " << edge[0] << ", " << edge[1] << " not found. Exiting..." << endl; 
        exit(1); 
    }

    triangle_t triangle = this->triangles_dict_[edge]; 
    for (int i = 0; i < 3; i++) {
        if (triangle[i] != edge[0] && triangle[i] != edge[1]) {
            return triangle[i]; 
        }
    }
    cout << "In Topology::get_third_vertex(): cannot find a third vertex. Exiting..." << endl;
    exit(1); 
}



// Determine whether an edge is in the current topology 
bool Topology::has_edge(edge_t edge) {
    return (this->triangles_dict_.count(edge) > 0); 
}



// Initialize the iterator for looping through all triangles
void Topology::start_iterator() {
    this->looping_ = true; 
    this->it_ = this->triangles_dict_.begin(); 
    this->visited_.clear(); 
}


// Determine whether there exists a next triangle to traverse
bool Topology::has_triangle() {
    return this->looping_; 
}


// Return the next triangle in the current topology 
// To loop through N triangles, the time complexity is O(N * logN)
triangle_t Topology::next_triangle() {
    if (!this->looping_) {
        cout << "In Topology::next_triangle(): No more triangle to traverse. Make sure to call Topology::start_iterator() before looping through triangles. Exiting..." << endl; 
        exit(1); 
    }

    triangle_t res = this->it_->second; 

    // Mark the current triangle as visited 
    this->visited_.insert(res); 

    // Increment iterator until the next triangle is not visited or until traversing the entire dictionary
    triangle_t curr = res; 
    while(this->visited_.count(curr) > 0 
          && this->it_ != this->triangles_dict_.end()) {
        this->it_++;    // Increment
        curr = this->it_->second; 
    }

    // Reset looping_ to false if reaching the end of iterator 
    if (this->it_ == this->triangles_dict_.end()) {
        this->looping_ = false;
    }

    return res; 
}



// Given a vertex, store all the connected in vertices in the vector `connected_vert`
void Topology::get_v2v(int vert_idx, vector<int>& connected_vert) {
    Vertices *pointset = this->pointset(); 

    for (int i = 0 ; i < pointset->nb(); i++) {
        if (this->has_edge({vert_idx, i}) || this->has_edge({i, vert_idx})) {
            connected_vert.push_back(i); 
        }
    }
}



// Similar to Topology::get_v2v, but only only get adjacent vertices whose geometric attributes have lower dimension
// See section 3.3 in (Caplan 2019) for explanation 
void Topology::get_v2v_along_entity(int vert_idx, vector<int>& connected_vert) {
    Vertices *pointset = this->pointset(); 

    for (int i = 0 ; i < pointset->nb(); i++) {
        if (this->has_edge({vert_idx, i}) || this->has_edge({i, vert_idx})) {
            short geom_v = pointset->get_geometry(vert_idx); 
            short geom_i = pointset->get_geometry(i); 
            
            // Condition under which we always add vertex i
            
            // 1. If the central vertex is in the interior 
            bool cond1 = (geom_v == 0); 

            // 2. If the central vertex is on boundary, and the vertex i is either on the same boundary (corner point included)
            bool cond2a = (
                geom_v == -1 && (
                    geom_i == -1
                    || geom_i == 1
                    || geom_i == 2
                )
            ); 

            bool cond2b = (
                geom_v == -2 && (
                    geom_i == -2
                    || geom_i == 2 
                    || geom_i == 3
                )
            ); 

            bool cond2c = (
                geom_v == -3 && (
                    geom_i == -3 
                    || geom_i == 3
                    || geom_i == 4
                )
            );

            bool cond2d = (
                geom_v == -4 && (
                    geom_i == -4 
                    || geom_i == 4
                    || geom_i == 1
                )
            ); 

            // Add vertex i if one of the conditions above is met 
            if (cond1 || cond2a || cond2b || cond2c || cond2d) {
                connected_vert.push_back(i); 
            }
        }
    }
}



// Get all undirected edges in the current topology 
set<edge_t>& Topology::get_undir_edges() {
    set<edge_t>& undir_edges = this->undir_edges_; 

    // Clear the previous set of edges 
    undir_edges.clear(); 

    for (auto it = this->triangles_dict_.begin(); it != this->triangles_dict_.end(); it++) {
        edge_t e_for = it->first; 
        edge_t e_back = {e_for[1], e_for[0]}; 
        
        // Add the edge (undirected) if it has not been added yet
        if (!undir_edges.count(e_for) && !undir_edges.count(e_back)) {
            undir_edges.insert(e_for); 
        }
    }

    return undir_edges; 
}



// Get all undirected edges in specified order of length
vector<edge_t>& Topology::get_undir_edges_ordered(MetricField *metricfield, bool ascending) {
    vector<edge_t>& undir_edges_vec = this->undir_edges_vec_; 

    // Clear the previous vector of edges
    undir_edges_vec.clear(); 

    // Copy all undirected edges from a set to a vector 
    for (auto edge : this->get_undir_edges()) {
        undir_edges_vec.push_back(edge); 
    }

    this->quicksort_undir_edges(0, undir_edges_vec.size() - 1, metricfield, ascending); 

    return undir_edges_vec; 
} 



// Quicksort undirected edges in specified order of length
void Topology::quicksort_undir_edges(int start_idx, int end_idx, MetricField *metricfield, bool ascending) {
    vector<edge_t>& undir_edges_vec = this->undir_edges_vec_; 

    if (end_idx <= start_idx) {return; }
    if (end_idx == start_idx + 1) {
        // Define the conditions of swapping two elements in a partition
        bool cond_asc = ascending && this->longer_than(undir_edges_vec[start_idx], undir_edges_vec[end_idx], metricfield); 
        bool cond_dsc = !ascending && this->shorter_than(undir_edges_vec[start_idx], undir_edges_vec[end_idx], metricfield); 

        if (cond_asc || cond_dsc) {
            iter_swap(undir_edges_vec.begin() + start_idx, undir_edges_vec.begin() + end_idx); 
        }
        return; 
    }

    int pi = this->partition_undir_edges(start_idx, end_idx, metricfield, ascending); 
    this->quicksort_undir_edges(start_idx, pi - 1, metricfield, ascending); 
    this->quicksort_undir_edges(pi + 1, end_idx, metricfield, ascending); 
}



// Partition as needed in quicksort 
int Topology::partition_undir_edges(int start_idx, int end_idx, MetricField *metricfield, bool ascending) {
    vector<edge_t>& undir_edges_vec = this->undir_edges_vec_; 

    // Choosing a pivot randomly  
    int chosen = rand() % (end_idx + 1 - start_idx) + start_idx; 
    iter_swap(undir_edges_vec.begin() + chosen, undir_edges_vec.begin() + end_idx); 

    // Indicate the position of right pivot so far
    int i = start_idx - 1; 

    // Main loop of partition
    for (int j = start_idx; j <= end_idx - 1; j++) {
        // Define the condition of swapping 
        bool cond_asc = ascending && this->shorter_than(undir_edges_vec[j], undir_edges_vec[end_idx], metricfield); 
        bool cond_dsc = !ascending && this->longer_than(undir_edges_vec[j], undir_edges_vec[end_idx], metricfield);

        if (cond_asc || cond_dsc) {
            i++; 
            iter_swap(undir_edges_vec.begin() + j, undir_edges_vec.begin() + i); 
        }
    }

    // Put the pivot in the right position 
    iter_swap(undir_edges_vec.begin() + (i+1), undir_edges_vec.begin() + end_idx); 

    return i + 1; 
}



// Compute the length of an edge_t under a metric field 
// Return Euclidean distance is metricfield is set to NULL
double Topology::get_length(edge_t edge, MetricField *metricfield) {
    Vertices *pointset = this->pointset(); 
    double *coord_start = pointset->get_vertex(edge[0]); 
    double *coord_end = pointset->get_vertex(edge[1]); 
    if (metricfield == NULL) {
        return get_distance_Euclidean(coord_start, coord_end); 
    }
    return metricfield->get_distance(coord_start, coord_end); 
}



// Determine whether len(e1) < len(e2) under metric
bool Topology::shorter_than(edge_t e1, edge_t e2, MetricField *metricfield) {
    return this->get_length(e1, metricfield) < this->get_length(e2, metricfield); 
}



// Determine whether len(e1) > len(e2) under metric
bool Topology::longer_than(edge_t e1, edge_t e2, MetricField *metricfield) {
    return this->get_length(e1, metricfield) > this->get_length(e2, metricfield); 
}



// Compute the area (volume) of a triangle under a metric field 
double Topology::get_volume(triangle_t triangle, MetricField *metricfield) {
    Vertices *pointset = this->pointset(); 
    double *coord1 = pointset->get_vertex(triangle[0]); 
    double *coord2 = pointset->get_vertex(triangle[1]); 
    double *coord3 = pointset->get_vertex(triangle[2]); 
    if (metricfield == NULL) {
        return get_volume_Euclidean(coord1, coord2, coord3); 
    }
    return metricfield->get_volume(coord1, coord2, coord3);
}



// Compute the mesh quality of a given triangle_t under a metric field
double Topology::get_quality(triangle_t triangle, MetricField *metricfield) {
    if (metricfield == NULL) {
        double volume = this->get_volume(triangle, NULL); 
        double dist1 = this->get_length({triangle[0], triangle[1]}, NULL); 
        double dist2 = this->get_length({triangle[0], triangle[2]}, NULL); 
        double dist3 = this->get_length({triangle[1], triangle[2]}, NULL); 
        return BETA * volume / (dist1 * dist1 + dist2 * dist2 + dist3 * dist3); 
    }
    
    Vertices *pointset = this->pointset(); 
    double *coord1 = pointset->get_vertex(triangle[0]); 
    double *coord2 = pointset->get_vertex(triangle[1]); 
    double *coord3 = pointset->get_vertex(triangle[2]); 
    return metricfield->get_quality(coord1, coord2, coord3); 
}





