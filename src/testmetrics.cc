#include <vector>
#include "testmetrics.h"
#include "metric.h"
#include <cmath>
#define _USE_MATH_DEFINES

using namespace std;
using namespace Eigen;  

// Define my own test set of metric fields 
vector<MetricField> testset_metrics{

    // h_x = 1, h_y = 2
    MetricField(
        CONSTRUCT_BY_PRINCIPALS, 
        COMPUTE_BY_METRIC, 
        [](double *coord)->Matrix2d {
            return Matrix2d::Identity();
        }, 
        [](double *coord)->Vector2d {
            return Vector2d(1, 2);  
        }
    ), 

    // // h_x = 1, h_y = 2 
    // // But constructed directly by a metric tensor
    // MetricField(
    //     CONSTRUCT_BY_METRIC_TENSOR, 
    //     COMPUTE_BY_METRIC, 
    //     [](double *coord)->Matrix2d {
    //         Matrix2d res; 
    //         res << 1, 0, 
    //                0, 0.25; 
    //         return res; 
    //     }
    // ), 

    // // h_x = 2.5, h_y = 0.2 * y + 0.2
    // // Dense at the bottom and sparse on the top 
    // MetricField(
    //     CONSTRUCT_BY_PRINCIPALS, 
    //     COMPUTE_BY_METRIC, 
    //     [](double *coord)->Matrix2d {
    //         return Matrix2d::Identity(); 
    //     }, 
    //     [](double *coord)->Vector2d {
    //         return Vector2d(2.5, 0.2 * coord[1] + 0.2); 
    //     }
    // ), 

    // // h_x = h_y = 0.2 * |y - x| + 0.4
    // // Dense around the line y = x and sparse elsewhere
    // MetricField(
    //     CONSTRUCT_BY_PRINCIPALS, 
    //     COMPUTE_BY_METRIC, 
    //     [](double *coord)->Matrix2d {
    //         return Matrix2d::Identity(); 
    //     }, 
    //     [](double *coord)->Vector2d {
    //         return Vector2d(0.2 * abs(coord[1] - coord[0]) + 0.4, 
    //                         0.2 * abs(coord[1] - coord[0]) + 0.4); 
    //     }
    // ), 

    // // Dense around the sine curve y = 2.5 * sin(0.2 * pi * x) + 5
    // MetricField(
    //     CONSTRUCT_BY_PRINCIPALS, 
    //     COMPUTE_BY_METRIC, 
    //     [](double *coord)->Matrix2d {
    //         // Set the principal directions as the normalized gradient and tangent vectors of the sine curve 
    //         Vector2d grad_normalized(0.5 * M_PI * cos(0.2 * M_PI * coord[0]), -1);
    //         Vector2d tang_normalized(1, 0.5 * M_PI * cos(0.2 * M_PI * coord[0])); 
    //         grad_normalized.normalize(); 
    //         tang_normalized.normalize(); 

    //         Matrix2d res; 
    //         res.col(0) = grad_normalized; 
    //         res.col(1) = tang_normalized; 
    //         return res; 
    //     }, 
    //     [](double *coord)->Vector2d {
    //         // Smaller principal length along gradient direction when closer to the sine curve
    //         // Constant principal length along tangent direction
    //         return Vector2d(0.2 + 0.5 * abs( coord[1] - 2.5*sin(0.2*M_PI*coord[0]) - 5 ), 1); 
    //     }
    // ),  

    // // Dense around the quater circle that passes (10, 0) and (0, 10)
    // MetricField(
    //     CONSTRUCT_BY_PRINCIPALS, 
    //     COMPUTE_BY_METRIC, 
    //     [](double *coord)->Matrix2d {
    //         // Special treatment at the origin 
    //         if (coord[0] == 0 && coord[1] == 0) {
    //             Matrix2d res; 
    //             res << sqrt(0.5), -sqrt(0.5), 
    //                    sqrt(0.5), sqrt(0.5); 
    //             return res; 
    //         }

    //         // Set the principal directions as the normalized gradient and tangent vectors of the circle
    //         Vector2d grad_normalized(coord[0], coord[1]);
    //         Vector2d tang_normalized(-coord[1], coord[0]); 
    //         grad_normalized.normalize(); 
    //         tang_normalized.normalize(); 

    //         Matrix2d res; 
    //         res.col(0) = grad_normalized; 
    //         res.col(1) = tang_normalized; 
        
    //         return res; 
    //     }, 
    //     [](double *coord)->Vector2d {
    //         // Smaller principal length along gradient direction when closer to the circle
    //         // Constant principal length along tangent direction
    //         return Vector2d(0.2 + 0.05 * abs( pow(coord[0], 2) + pow(coord[1], 2) - 100 ), 1); 
    //     }
    // ),  

}; 



vector<Embedding> testset_embeddings = {
    // // z(x, y) = 5 * tanh(0.5 * (y - cos(pi * x / 5) - 5) )
    // Embedding(
    //     [](double *coord)->double {
    //         return 5 * tanh(0.5 * (coord[1] - cos(M_PI * coord[0] / 5) - 5) ); 
    //     }, 
    //     "$z(x, y) = 5\\tanh\\left( 0.5 \\cdot (y - \\cos(\\pi x / 5) - 5) \\right)$", 
    //     [](double *coord)->Matrix2d {
    //         Matrix2d res; 
    //         double x = coord[0], y = coord[1], pi = M_PI; 
    //         res << 0.25*pow(pi, 2)*pow(1 - pow(tanh(0.5*y - 0.5*cos((1.0/5.0)*pi*x) - 2.5), 2), 2)*pow(sin((1.0/5.0)*pi*x), 2) + 1, 
    //             0.5*pi*(1 - pow(tanh(0.5*y - 0.5*cos((1.0/5.0)*pi*x) - 2.5), 2))*(2.5 - 2.5*pow(tanh(0.5*y - 0.5*cos((1.0/5.0)*pi*x) - 2.5), 2))*sin((1.0/5.0)*pi*x), 
    //             0.5*pi*(1 - pow(tanh(0.5*y - 0.5*cos((1.0/5.0)*pi*x) - 2.5), 2))*(2.5 - 2.5*pow(tanh(0.5*y - 0.5*cos((1.0/5.0)*pi*x) - 2.5), 2))*sin((1.0/5.0)*pi*x), 
    //             6.25*pow(1 - pow(tanh(0.5*y - 0.5*cos((1.0/5.0)*pi*x) - 2.5), 2), 2) + 1; 
    //         return res; 
    //     }
    // ), 

    // // z(x, y) = 5 * tanh(1.0 * (y - cos(pi * x / 5) - 5) )
    // Embedding(
    //     [](double *coord)->double {
    //         return 5 * tanh(1.0 * (coord[1] - cos(M_PI * coord[0] / 5) - 5) ); 
    //     }, 
    //     "$z(x, y) = 5\\tanh\\left( 1.0 \\cdot (y - \\cos(\\pi x / 5) - 5) \\right)$", 
    //     [](double *coord)->Matrix2d {
    //         Matrix2d res; 
    //         double x = coord[0], y = coord[1], pi = M_PI; 
    //         res << pow(pi, 2)*pow(1 - pow(tanh(y - cos((1.0/5.0)*pi*x) - 5), 2), 2)*pow(sin((1.0/5.0)*pi*x), 2) + 1, 
    //             pi*(1 - pow(tanh(y - cos((1.0/5.0)*pi*x) - 5), 2))*(5 - 5*pow(tanh(y - cos((1.0/5.0)*pi*x) - 5), 2))*sin((1.0/5.0)*pi*x), 
    //             pi*(1 - pow(tanh(y - cos((1.0/5.0)*pi*x) - 5), 2))*(5 - 5*pow(tanh(y - cos((1.0/5.0)*pi*x) - 5), 2))*sin((1.0/5.0)*pi*x), 
    //             pow(5 - 5*pow(tanh(y - cos((1.0/5.0)*pi*x) - 5), 2), 2) + 1; 
    //         return res; 
    //     }
    // ), 


    // // z(x, y) = 5 * tanh(3.0 * (y - cos(pi * x / 5) - 5) )
    // Embedding(
    //     [](double *coord)->double {
    //         return 5 * tanh(3.0 * (coord[1] - cos(M_PI * coord[0] / 5) - 5) ); 
    //     }, 
    //     "$z(x, y) = 5\\tanh\\left( 3.0 \\cdot (y - \\cos(\\pi x / 5) - 5) \\right)$", 
    //     [](double *coord)->Matrix2d {
    //         Matrix2d res; 
    //         double x = coord[0], y = coord[1], pi = M_PI; 
    //         res << 9*pow(pi, 2)*pow(1 - pow(tanh(3*y - 3*cos((1.0/5.0)*pi*x) - 15), 2), 2)*pow(sin((1.0/5.0)*pi*x), 2) + 1, 
    //             3*pi*(1 - pow(tanh(3*y - 3*cos((1.0/5.0)*pi*x) - 15), 2))*(15 - 15*pow(tanh(3*y - 3*cos((1.0/5.0)*pi*x) - 15), 2))*sin((1.0/5.0)*pi*x), 
    //             3*pi*(1 - pow(tanh(3*y - 3*cos((1.0/5.0)*pi*x) - 15), 2))*(15 - 15*pow(tanh(3*y - 3*cos((1.0/5.0)*pi*x) - 15), 2))*sin((1.0/5.0)*pi*x), 
    //             pow(15 - 15*pow(tanh(3*y - 3*cos((1.0/5.0)*pi*x) - 15), 2), 2) + 1; 
    //         return res; 
    //     }
    // ), 

    // // z(x, y) = 5 * tanh(5.0 * (y - cos(pi * x / 5) - 5) )
    // Embedding(
    //     [](double *coord)->double {
    //         return 5 * tanh(5.0 * (coord[1] - cos(M_PI * coord[0] / 5) - 5) ); 
    //     }, 
    //     "$z(x, y) = 5\\tanh\\left( 5.0 \\cdot (y - \\cos(\\pi x / 5) - 5) \\right)$", 
    //     [](double *coord)->Matrix2d {
    //         Matrix2d res; 
    //         double x = coord[0], y = coord[1], pi = M_PI; 
    //         res << 25*pow(pi, 2)*pow(1 - pow(tanh(5*y - 5*cos((1.0/5.0)*pi*x) - 25), 2), 2)*pow(sin((1.0/5.0)*pi*x), 2) + 1, 
    //             5*pi*(1 - pow(tanh(5*y - 5*cos((1.0/5.0)*pi*x) - 25), 2))*(25 - 25*pow(tanh(5*y - 5*cos((1.0/5.0)*pi*x) - 25), 2))*sin((1.0/5.0)*pi*x), 
    //             5*pi*(1 - pow(tanh(5*y - 5*cos((1.0/5.0)*pi*x) - 25), 2))*(25 - 25*pow(tanh(5*y - 5*cos((1.0/5.0)*pi*x) - 25), 2))*sin((1.0/5.0)*pi*x), 
    //             pow(25 - 25*pow(tanh(5*y - 5*cos((1.0/5.0)*pi*x) - 25), 2), 2) + 1; 
    //         return res; 
    //     }
    // ), 

    
    // // z(x, y) = 0.1 * x^2
    // Embedding(
    //     [](double *coord)->double {
    //         return 0.1 * coord[0] * coord[0]; 
    //     }, 
    //     "$z(x, y) = 0.1x^2$", 
    //     [](double *coord)->Matrix2d {
    //         Matrix2d res; 
    //         double x = coord[0], y = coord[1], pi = M_PI; 
    //         res << 1 + 4 * 0.1 * 0.1 * x * x, 0, 
    //                0, 1; 
    //         return res; 
    //     }
    // ), 

    // // z(x, y) = 0.2 * x^2
    // Embedding(
    //     [](double *coord)->double {
    //         return 0.2 * coord[0] * coord[0]; 
    //     }, 
    //     "$z(x, y) = 0.2x^2$", 
    //     [](double *coord)->Matrix2d {
    //         Matrix2d res; 
    //         double x = coord[0], y = coord[1], pi = M_PI; 
    //         res << 1 + 4 * 0.2 * 0.2 * x * x, 0, 
    //                0, 1; 
    //         return res; 
    //     }
    // ), 

    // // z(x, y) = 0.3 * x^2
    // Embedding(
    //     [](double *coord)->double {
    //         return 0.3 * coord[0] * coord[0]; 
    //     }, 
    //     "$z(x, y) = 0.3x^2$", 
    //     [](double *coord)->Matrix2d {
    //         Matrix2d res; 
    //         double x = coord[0], y = coord[1], pi = M_PI; 
    //         res << 1 + 4 * 0.3 * 0.3 * x * x, 0, 
    //                0, 1; 
    //         return res; 
    //     }
    // ), 

    // z(x, y) = 0.4 * x^2
    Embedding(
        [](double *coord)->double {
            return 0.4 * coord[0] * coord[0]; 
        }, 
        "$z(x, y) = 0.4x^2$", 
        [](double *coord)->Matrix2d {
            Matrix2d res; 
            double x = coord[0], y = coord[1], pi = M_PI; 
            res << 1 + 4 * 0.4 * 0.4 * x * x, 0, 
                   0, 1; 
            return res; 
        }
    ),
}; 




