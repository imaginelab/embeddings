#ifndef PREDICATES_H_
#define PREDICATES_H_

#define REAL double
extern "C" {
    void exactinit(); 
	REAL orient2d( REAL* pa , REAL* pb, REAL* pc );
}
#endif
