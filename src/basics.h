#include "metric.h"
#include <vector> 
#include <array>
#include <map> 
#include <set>

using namespace std;  

#define DET_THRESHOLD 1e-8

#ifndef BASICS_H
#define BASICS_H

typedef array<int, 3> triangle_t; 
typedef array<int, 2> edge_t; 


// A data structure that stores all vertices
class Vertices {
public:
    Vertices(int amb_dim_arg); 
    int nb();    // return the number of vertices

    // Setters and getters 
    double *get_vertex(int idx); 
    void add_vertex(double *coord); 
    short get_geometry(int idx); 
    void add_geometry(short geom);
    int amb_dim(); 
    int amb_dim(int amb_dim_arg);  

    // Helper function for local operators
    short infer_geometry(edge_t edge);
    void remove(int idx); 
    bool exists(int idx); 


private: 
    // Total size = amb_dim_ * number of dimensions 
    // The coordinate of the i^th vertex are stored at index i * amb_dim_, i * amb_dim_ + 1, ... i * amb_dim_ + (amb_dim_ - 1)
    vector<double> vertices_;   

    // Indicates whether a vertex is at the corner, the boundary or the interior
    vector<short> geometry_; 

    // Ambient dimension == the dimensionality of the space in which a surface is embedded 
    int amb_dim_; 

    // The collection of vertices that are removed 
    set<int> removed_; 
}; 



// A data structure that stores all trianglulation and refers to the relevant pointset 
class Topology {
public: 
    Topology(Vertices *pointset_arg); 
    int nb();   //  Number of triangles

    // Setters and getters 
    Vertices *pointset(); 
    triangle_t get_triangle(edge_t edge); 
    void add_triangle(int vert_1_idx, int vert_2_idx, int vert_3_idx); 
    void delete_triangle(edge_t edge); 

    // Helper functions (general)
    int get_third_vertex(edge_t edge); 
    bool has_edge(edge_t edge);  

    // Iterator-like methods that traverses each triangle 
    void start_iterator();   // Always call this before looping through triangles
    bool has_triangle(); 
    triangle_t next_triangle();

    // Helper functions for local operators 
    void get_v2v(int vert_idx, vector<int>& connected_vert);
    void get_v2v_along_entity(int vert_idx, vector<int>& connected_vert); 
    set<edge_t>& get_undir_edges(); 
    vector<edge_t>& get_undir_edges_ordered(MetricField *metricfield, bool ascending = true); 
    void quicksort_undir_edges(int start_idx, int end_idx, MetricField *metricfield, bool ascending = true); 
    int partition_undir_edges(int start_idx, int end_idx, MetricField *metricfield, bool ascending = true); 

    // Helper functions for distances and metrics 
    double get_length(edge_t edge, MetricField *metricfield); 
    bool shorter_than(edge_t e1, edge_t e2, MetricField *metricfield); 
    bool longer_than(edge_t e1, edge_t e2, MetricField *metricfield); 
    double get_volume(triangle_t triangle, MetricField *metricfield); 
    double get_quality(triangle_t triangle, MetricField *metricfield); 
    

    // Local operators 
    // Return 0 if success 
    unsigned short edge_split(edge_t edge, double proportion = 0.5);  
    unsigned short edge_swap(edge_t edge);   
    unsigned short vertex_removal(int vert_idx, MetricField *metricfield); 
    unsigned short vertex_smoothing(int vert_idx, MetricField *metricfield); 

private: 
    
    // * Core data structures * 
    
    Vertices *pointset_;    // The collection of vertices relevant to this topology
    map<edge_t, triangle_t> triangles_dict_;    // A dictionary of triangles. The keys are directed edges (counterclockwise direction)
    
    // * Helper data structures *
    
    set<triangle_t> visited_;    // A set for keeping track whether a triangle is traversed 
    bool looping_ = false;    // Indicate whether we are currently looping through triangles 
    map<edge_t, triangle_t>::iterator it_;    // Iterator for the triangle dictionary
    set<edge_t> undir_edges_;    // The set of all undirected edges
    vector<edge_t> undir_edges_vec_; 
}; 





#endif 