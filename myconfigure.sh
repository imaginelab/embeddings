#!/bin/bash

# This script must be run under the project root directory 

TOOLS="./tools/visualize_mesh_02.py ./tools/visualize_mesh_03.py"

# Rerun cmake on the project
cmake .
cmake ./build/debug

# Copy necessary tools and scripts to the same directory as exectuable file
for tool in $TOOLS 
    do
    # Get only the file name and discard directory name
    IFS='/' read -ra array <<< "$tool"
    filename=${array[-1]}

    # The target path of copying
    target="./build/debug/bin/${filename}"

    # Copy
    cp $tool $target
    echo "Copied ${tool} to ${target}"
done 


