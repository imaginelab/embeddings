Embedding functions: 
* $z(x, y) = Ax^2, A = 0.1, 0.2, 0.3$
* $z(x, y) = 5\tanh\left( 5.0 \cdot (y - \cos(\pi x / 5) - 5) \right)$

Types of analysis: 

* Heat map of mesh & histogram of qualities 
    - Compute by metric, eval by metric 
    - Compute by embedding, eval by metric
    - Compute by metric, eval by embedding
    - Compute by embedding, eval by embedding 

* 3D Plot of embedding 

* Runtime analysis 
    - Print out runtime in the program 
    - Compute how many triangles are expected for each case 